import numpy as np
from scipy.optimize import curve_fit
from tqdm.auto import tqdm
from load_data import load_GeneraliseRecouplingData, get_sb_lims

def extract_angular_phases(XUV_freq="0.9830", SB=16, path='./', component='all'):
    """
    Arguments: 
        - XUV_freq:  <str>
                     The frequency of the central XUV harmonic used in the 
                     RABBITT calculation; this is used to load the correct
                     data sets and to identify the sideband limits.

        - SB:        <int>
                     The sideband to be investigated, identified as a multiple
                     of the driving IR frequency.

        - path:      <str>
                     The relative or absolute path to the folder 
                     containing the time-delayed photoelectron spectra
                     output from GeneraliseRecoupling.

        - component: <str>
                     The component of the photoelectron spectra to be plotted.
                     Options are: 
                         'all' - the total delay-averaged photoelectron spectrum
                         'J1/2' - only the J=1/2 Spin-Orbit component contributions
                         'J3/2' - only the J=3/2 Spin-Orbit component contributions
    Returns: 
        - phase_values_superset: <list <list <float>>>
                        The phase values calculated at each momentum point,
                        for each emission angle

        - phase_uncertainties_superset: <list <list <float>>>
                               The uncertainty associated with each phase value,
                               for each emission angle

        - momenta: <list <float>>
                   The indices of the momentum grid matching each phase value 
        - dK: <float>
                   The momentum grid spacing
    """

    assert type(XUV_freq) is str, "XUV_freq must be a string (form 0.1234, in a.u.)"
    assert type(SB) is int, "SB must be an int (and specified in sb_lims in load_data.py"

    data, momentum_range = load_GeneraliseRecouplingData(path, component)
    dK = momentum_range[1] - momentum_range[0]
    number_of_angles = data[0].shape[0]
    included_angles = [i for i in range(number_of_angles-1)] # edit this to select angles
    num_included_angles = len(included_angles)

    chosen_momentum_start, chosen_momentum_end = get_sb_lims(SB, XUV_freq, component.lower())
    scale_factor = 1e18 # scales up sideband intensity uniformly to aid curve fitting procedure

    momenta = []
    phase_values_superset = [[] for a in range(num_included_angles)] 
    phase_uncertainties_superset = [[] for a in range(num_included_angles)]

    def do_angular_momentum(k):
        """
        Calculate the spectral phase associated with a single momentum grid point,
        for each emission angle
        """
        chosen_momentum_index = chosen_momentum_start + k
        momenta.append(chosen_momentum_index)

        peaks_superset = [[] for a in range(num_included_angles)]

        for snap in data:
            for angle_index,angle in enumerate(included_angles):
                peaks_superset[angle_index].append(snap[angle,chosen_momentum_index]*scale_factor)

        def test_func(t, a, b, c):
            """
            Form of curve to be fitted to sideband as a function of delay.
            Note: delays given in terms of IR period, so 2*t is used 
                  rather than 2*W_IR*t
            """
            return a * np.cos(2*t + b) + c

        phase_delays = [(j-8)*np.pi/8 for j in range(len(data))]

        for angle_index,peaks in enumerate(peaks_superset):
            params, params_covariance = curve_fit(test_func,
                                                    np.array(phase_delays),
                                                    np.array(peaks),
                                                    p0 = (np.max(np.abs(peaks)),
                                                        0.0,
                                                        np.mean(peaks)
                                                        ),
                                                    bounds = ([0.0,
                                                                -np.pi,
                                                                -np.inf],
                                                            [np.inf,
                                                                np.pi,
                                                                np.inf]
                                                            ),
                                                    maxfev=100000,
                                                    method='trf'
                                                    )
            if False: # Set to True / condition to investigate the curve fitting
                plt.figure()
                plt.plot(phase_delays, np.array(peaks), '--.',label='real')
                fitted_vals = [test_func(i, *params) for i in phase_delays]
                plt.plot(phase_delays, fitted_vals, '--x',label='fitted')
                plt.legend()
                plt.xlabel('Phase Delay (relative to IR)')
                plt.ylabel('Intensity (arb.)')
                plt.title(chosen_energy_index)
                plt.show()
            phase_values_superset[angle_index].append(params[1]) 
            phase_uncertainties_superset[angle_index].append(np.sqrt(np.diag(params_covariance)[1]))

    num_momentum_pts = chosen_momentum_end-chosen_momentum_start
    pts = [k for k in range(num_momentum_pts)]
    for k in tqdm(pts, leave=True, desc="Extracting Phases"):
        do_angular_momentum(k)
    
    momenta_au = [momentum_range[k] for k in momenta]

    return phase_values_superset, phase_uncertainties_superset, momenta_au

def extract_1D_phases(XUV_freq="0.9830", SB=16, path='./', component='all'):
    """
    Arguments: 
        - XUV_freq:  <str>
                     The frequency of the central XUV harmonic used in the 
                     RABBITT calculation; this is used to load the correct
                     data sets and to identify the sideband limits.

        - SB:        <int>
                     The sideband to be investigated, identified as a multiple
                     of the driving IR frequency.

        - path:      <str>
                     The relative or absolute path to the folder 
                     containing the time-delayed photoelectron spectra
                     output from GeneraliseRecoupling.

        - component: <str>
                     The component of the photoelectron spectra to be plotted.
                     Options are: 
                         'all' - the total delay-averaged photoelectron spectrum
                         'J1/2' - only the J=1/2 Spin-Orbit component contributions
                         'J3/2' - only the J=3/2 Spin-Orbit component contributions
    Returns: 
        - phase_values: <list <float>>
                        The phase values calculated at each momentum point

        - phase_uncertainties: <list <float>>
                               The uncertainty associated with each phase value

        - momenta: <list <float>>
                   The indices of the momentum grid matching each phase value 
        - dK: <float>
                   The momentum grid spacing
    """
    assert type(XUV_freq) is str, "XUV_freq must be a string (form 0.1234, in a.u.)"
    assert type(SB) is int, "SB must be an int (and specified in sb_lims in load_data.py"

    data, momentum_range = load_GeneraliseRecouplingData(path, component)
    dK = momentum_range[1] - momentum_range[0]
    number_of_angles = data[0].shape[0]
    integrated_data = np.array([np.trapz([2*np.pi*np.sin(ang)*vals for ang,vals in zip(np.linspace(0,np.pi,181),snap)],
                           dx=np.pi/180.0, 
                           axis=0) for snap in data])

    chosen_momentum_start, chosen_momentum_end = get_sb_lims(SB, XUV_freq, component.lower())
    scale_factor = 1e16 # scales up sideband intensity uniformly to aid curve fitting procedure

    momenta = []
    phase_values = [] 
    phase_uncertainties = [] 

    def do_momentum(k):
        """
        Calculate the spectral phase associated with a single momentum grid point
        """
        chosen_momentum_index = chosen_momentum_start + k
        momenta.append(chosen_momentum_index)

        peaks = []

        for snap in integrated_data:
            peaks.append(snap[chosen_momentum_index]*scale_factor)

        def test_func(t, a, b, c):
            """
            Form of curve to be fitted to sideband as a function of delay.
            Note: delays given in terms of IR period, so 2*t is used 
                  rather than 2*W_IR*t
            """
            return a * np.cos(2*t + b) + c

        phase_delays = [(j-8)*np.pi/8 for j in range(len(data))]

        params, params_covariance = curve_fit(test_func,
                                                np.array(phase_delays),
                                                np.array(peaks),
                                                p0 = (np.max(np.abs(peaks)),
                                                    0.0,
                                                    np.mean(peaks)
                                                    ),
                                                bounds = ([0.0,
                                                            -np.pi,
                                                            -np.inf],
                                                        [np.inf,
                                                            np.pi,
                                                            np.inf]
                                                        ),
                                                maxfev=100000,
                                                method='trf'
                                                )
        if False: # Set to True / condition to investigate the curve fitting
            plt.figure()
            plt.plot(phase_delays, np.array(peaks), '--.',label='real')
            fitted_vals = [test_func(i, *params) for i in phase_delays]
            plt.plot(phase_delays, fitted_vals, '--x',label='fitted')
            plt.legend()
            plt.xlabel('Phase Delay (relative to IR)')
            plt.ylabel('Intensity (arb.)')
            plt.title(chosen_energy_index)
            plt.show()
        phase_values.append(params[1]) 
        phase_uncertainties.append(np.sqrt(np.diag(params_covariance)[1]))

    num_momentum_pts = chosen_momentum_end-chosen_momentum_start
    pts = [k for k in range(num_momentum_pts)]
    for k in tqdm(pts, leave=True, desc="Extracting Phases"):
        do_momentum(k)
    
    momenta_au = [momentum_range[k] for k in momenta]

    return phase_values, phase_uncertainties, momenta_au


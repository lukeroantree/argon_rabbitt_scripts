# Argon\_RABBITT\_Scripts

Example data & code for replicating results in the paper 

"Triple-resolution of spectral phases via semi-relativistic ab-initio RABBITT simulations"

The full dataset and codes for reproducibility are available on Zenodo (DOI: 10.5281/zenodo.7148018)

#### Try it out online now with Binder (may take 30-60s to start): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/lukeroantree%2Fargon_rabbitt_scripts/HEAD?labpath=Demo_Phase_Extractions.ipynb) 

#### Authors and acknowledgment
- [Luke Roantree](mailto:lroantree01@qub.ac.uk)
- Jack Wragg
- Connor Ballance
- Hugo van der Hart
- [Andrew Brown](mailto:andrew.brown@qub.ac.uk)

(all Queen's University Belfast)

---

The code provided in this repository is designed to operate on photoelectron momenta files produced by the RMT code suite. 

The RMT code is part of the UK-AMOR suite, and can be obtained for free **[here](https://gitlab.com/Uk-amor/RMT)**. 

This work benefited from computational support by CoSeC, the Computational Science Centre for Research Communities, through CCPQ. 

The authors acknowledge funding from the UK Engineering and Physical Sciences Research Council (EPSRC) under grants EP/P022146/1, EP/P013953/1, EP/R029342/1, EP/T019530/1, and EP/V05208X/1. 

This work relied on the [ARCHER2 UK National Supercomputing Service](https://archer2.ac.uk), for which access was obtained via the UK-AMOR consortium funded by EPSRC

## License
TBD

## Requirements
    - Photoelectron momentum-space spectra produced by GeneraliseRecoupling, operating on RMT output files
    - Python 3.6+
    - NumPy, SciPy, Matplotlib, tqdm (v. 4.30+), jupyter

A minimalist conda virtual environment with the required packages and versions 
can be built from the environment.yml file provided.

To create the environment (named argon\_min), run

    conda env create -f environment.yml

and to activate the environment, run

    conda activate argon_min

To launch the demo notebook, run

    jupyter notebook Demo_Phase_Extractions.ipynb

The demonstration notebook presents the software tools used in the paper in the context of a simpler investigation; investigating the how suitable sideband SB14 is to act as a reference.

An ideal reference sideband is free of any imprints from atomic structure, but is affected by pulse chirp and jitter in the same manner as the other sidebands. We expect not to need a reference sideband due to the effects of chirp and jitter in our spectra being negligible by construction of our pulses, an assumption validated in this mini-investigation.

To extend the demo notebook to investigate other sidebands / data corresponding to other driving IR frequencies, use the `plot_delay_averaged_sideband` function with `axis='eV'` to investigate where the sidebands are, and then again for a narrower energy range with `axis='indices'` to identify the indices of the upper and lower sideband limits. Add these to the `sb_lims` dictionary in `load_data.py` in order to be able to extract phases. 

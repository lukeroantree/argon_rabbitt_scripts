import numpy as np
import matplotlib.pyplot as plt
from extract_phases import extract_angular_phases, extract_1D_phases
from load_data import load_GeneraliseRecouplingData


mom_au_to_en_ev = lambda k: (0.5*k**2)*27.2114    

def _get_index_nearest_energy(energies, desired_energy):
    """
    Internal tool. Finds the index of the energy point nearest
    a specified energy, for a given energy grid.

    Arguments: 
        - energies:  <numpy array>
                     The energy interval to search, in eV

        - desired_energy: <int, float>
                     The energy to be matched, in eV
    Returns:
        - <int>: The index of the element of 'energies' with  the
                 closest value to 'desired_energy'
    """
    transformed_array = energies - desired_energy
    return np.argmin(np.abs(transformed_array))

def plot_delay_averaged_sideband(lower_eV, upper_eV, path='./', component='all', axis='eV'):
    """
    Plot the IR-APT delay averaged sideband yield as a function of 
    photoelectron energy.

    Arguments: 
        - lower_eV:  <int,float> 
                     The lowest photoelectron energy to be plotted.
                     The plotted figure may not exactly match the value 
                     specified here, instead the nearest point on the 
                     energy grid will be used.

        - upper_eV:  <int,float> 
                     The highest photoelectron energy to be plotted, in eV.
                     The plotted figure may not exactly match the value 
                     specified here, instead the nearest point on the 
                     energy grid will be used.

        - path:      <str>
                     The relative or absolute path to the folder 
                     containing the time-delayed photoelectron spectra
                     output from GeneraliseRecoupling.

        - component: <str>
                     The component of the photoelectron spectra to be plotted.
                     Options are: 
                         'all' - the total delay-averaged photoelectron spectrum
                         'J1/2' - only the J=1/2 Spin-Orbit component contributions
                         'J3/2' - only the J=3/2 Spin-Orbit component contributions

        - axis:      <str>
                     The desired scale for the x-axis.
                     Options are:
                         'eV' - shown in eV, useful for identifying sidebands
                         'indicies' - raw grid points
    Returns: 
        N/A

    """
    assert type(axis) is str, "axis must be a string"
    assert axis.lower() in ('ev', 'indices'), "axis must be one of ('eV', 'indices')"

    data, momentum_range = load_GeneraliseRecouplingData(path, component)

    energy_range = mom_au_to_en_ev(np.array(momentum_range)) 

    start_idx = _get_index_nearest_energy(energy_range, lower_eV)
    end_idx = _get_index_nearest_energy(energy_range, upper_eV)

    if axis.lower()=='ev':
        X = energy_range[start_idx:end_idx+1]
    elif axis.lower()=='indices':
        X = [i for i in range(start_idx,end_idx+1)]

    averaged_data = np.sum(data, axis=0)/len(data)
    averaged_integrated_data = np.array([np.trapz([2*np.pi*np.sin(np.pi*ang/180)*vals for
                                                         ang,vals in enumerate(d[:180])], 
                                                        dx=np.pi/180.0, 
                                                        axis=0) for
                                               d in averaged_data.T])
    Y = averaged_integrated_data[start_idx:end_idx+1]
    fig = plt.figure(10,figsize=(14,10))

    # using some values from the energy-scan figure in cell above
    plt.plot(X, Y)
    plt.xlabel('Photoelectron Energy (eV)' if axis.lower()=='ev' else 'Photoelectron Energy (grid indices)', weight='normal')
    plt.ylabel('Intensity (arbitrary)', weight='normal')
    plt.grid(True)
    plt.show()
    
def plot_spectrogram(lower_eV, upper_eV, path='./', component='all', axis='eV', log=False):
    assert type(axis) is str, "axis must be a string"
    assert axis.lower() in ('ev', 'indices'), "axis must be one of ('eV', 'indices')"

    data, momentum_range = load_GeneraliseRecouplingData(path, component)

    energy_range = mom_au_to_en_ev(np.array(momentum_range)) 

    start_idx = _get_index_nearest_energy(energy_range, lower_eV)
    end_idx = _get_index_nearest_energy(energy_range, upper_eV)

    if axis.lower()=='ev':
        X = energy_range[start_idx:end_idx+1]
    elif axis.lower()=='indices':
        X = [i for i in range(start_idx,end_idx+1)]
        
    integrated_data = np.array([[np.trapz([2*np.pi*np.sin(np.pi*ang/180)*vals for
                                                         ang,vals in enumerate(e[:180])], 
                                                        dx=np.pi/180.0, 
                                                        axis=0) for
                                               e in d.T] 
                                for d in data])
    Y = np.array([i*(np.pi/8)*0.41704900930926 for i in range(-8,9,1)])
    Z = integrated_data[:,start_idx:end_idx+1] 
    # Hugo correction:
    for snap in range(Z.shape[0]):
        Z[snap] /= np.array(momentum_range)[start_idx:end_idx+1]
    
    fig = plt.figure(figsize=(10,6))
    ax = fig.add_subplot(111)
    ax.grid(False)
    if log:
        Z = np.where(np.log(Z)<-11.5, -11.5, np.log(Z))

    spect = ax.contourf(X, Y, Z, levels=40, cmap=plt.get_cmap('viridis'))

    ax.set_ylabel('Phase Delay (fs)', labelpad=8, fontsize=25)
    fig.subplots_adjust(right=0.8, wspace=0, hspace=0.01)
    cbar_ax = fig.add_axes([0.805, 0.15, 0.015, 0.7])
    cb = fig.colorbar(plt.cm.ScalarMappable(cmap=plt.get_cmap('viridis')), 
                      cax=cbar_ax, 
                      ticks=[i/10 for i in range(11)])
    cb.set_label(f"Intensity ({'Logscale, ' if log else ''}Normalized)", labelpad=13, fontsize=25)
    cb.ax.tick_params(labelsize=18)

    for c in spect.collections:
        c.set_edgecolor("face")

    ax.set_xlabel('Photoelectron Energy (eV)' if axis.lower()=='ev' else 'Photoelectron Energy (grid indices)', fontsize=25, labelpad=5)
    ax.yaxis.set_tick_params(labelsize=18)
    ax.xaxis.set_tick_params(labelsize=18)
    plt.show()

def plot_phase_vs_angle_for_energies(XUV_freq="0.9830", SB=16, path='./', component='all', desired_points_eV=(9.4,9.5,9.6)):
    """
    Calculates the emission-angle resolved spectral phase across a chosen 
    sideband, and plots the spectral phase as a function of emission
    angle at the specified energies.

    Arguments: 
        - XUV_freq:  <str>
                     The frequency of the central XUV harmonic used in the 
                     RABBITT calculation; this is used to load the correct
                     data sets and to identify the sideband limits.

        - SB:        <int>
                     The sideband to be investigated, identified as a multiple
                     of the driving IR frequency.

        - path:      <str>
                     The relative or absolute path to the folder 
                     containing the time-delayed photoelectron spectra
                     output from GeneraliseRecoupling.

        - component: <str>
                     The component of the photoelectron spectra to be plotted.
                     Options are: 
                         'all' - the total delay-averaged photoelectron spectrum
                         'J1/2' - only the J=1/2 Spin-Orbit component contributions
                         'J3/2' - only the J=3/2 Spin-Orbit component contributions

        - desired_pts_eV: <list-like <int, float>>
                     The energy values for which to evaluate the spectral phase as
                     a function of emission angle. Values are given in eV, and are
                     mapped internally to the closest energy grid point.
    Returns: 
        N/A
    """
    phase_values_superset, phase_uncertainties_superset, momenta = extract_angular_phases(XUV_freq, SB, path, component)

    SB_energy_pts = mom_au_to_en_ev(np.array(momenta)) 

    fig = plt.figure(10,figsize=(14,10))
    desired_points_indices = (_get_index_nearest_energy(SB_energy_pts, desired_point) for desired_point in desired_points_eV)
    for energy_index in desired_points_indices:
        energy = SB_energy_pts[energy_index]
        angular_phases = np.array([phase_values_superset[i][energy_index] for i in range(180)])
        angular_uncertainties = np.array([phase_uncertainties_superset[i][energy_index] for i in range(180)])

        #Shift by +-2Pi if needed
        corrected_phase = [angular_phases[0]]
        for k in angular_phases[1:]:
            if corrected_phase[-1]-k > np.pi:
                corrected_phase.append(k + 2*np.pi)
            elif k-corrected_phase[-1] > np.pi:
                corrected_phase.append(k - 2*np.pi)
            else:
                corrected_phase.append(k)
        
        plt.plot(np.linspace(0,np.pi,180),
                    corrected_phase,
                    '-',
                    alpha=1,
                    label='{:.3f}eV'.format(energy))
        plt.fill_between(np.linspace(0,np.pi,180),
                            np.array(corrected_phase) +
                            np.array(angular_uncertainties),
                            np.array(corrected_phase) -
                            np.array(angular_uncertainties),
                            color='grey',
                            alpha=0.2)
    plt.margins(x=0)
    plt.xlabel('Emission Angle (radians)', weight='normal')
    plt.ylabel('Extracted Phase (radians)', weight='normal')
    plt.grid(True)
    plt.legend(loc=3,ncol=1, fancybox=True, shadow=True)
    plt.show()

def plot_phase_vs_energy_for_angles(XUV_freq="0.9830", SB=16, path='./', component='all', desired_angles=(0,20, 55, 75, 90)):
    """
    Calculates the emission-angle resolved spectral phase across a chosen 
    sideband, and plots the spectral phase as a function of energy 
    at the specified emission angles.

    Arguments: 
        - XUV_freq:  <str>
                     The frequency of the central XUV harmonic used in the 
                     RABBITT calculation; this is used to load the correct
                     data sets and to identify the sideband limits.

        - SB:        <int>
                     The sideband to be investigated, identified as a multiple
                     of the driving IR frequency.

        - path:      <str>
                     The relative or absolute path to the folder 
                     containing the time-delayed photoelectron spectra
                     output from GeneraliseRecoupling.

        - component: <str>
                     The component of the photoelectron spectra to be plotted.
                     Options are: 
                         'all' - the total delay-averaged photoelectron spectrum
                         'J1/2' - only the J=1/2 Spin-Orbit component contributions
                         'J3/2' - only the J=3/2 Spin-Orbit component contributions

        - desired_angles: <list-like <int>>
                     The emission-angles along which to evaluate the spectral phase 
                     as a function of photoelectron energy. Values are given in 
                     degrees, and must be integers in the interval [0,180].
    Returns: 
        N/A
    """
    phase_values_superset, phase_uncertainties_superset, momenta = extract_angular_phases(XUV_freq, SB, path, component)

    X = mom_au_to_en_ev(np.array(momenta)) 

    plt.close('all')
    fig = plt.figure(10,figsize=(14,8))

    for angle_index, angle in enumerate(desired_angles):
        energy_phases = phase_values_superset[angle]
        corrected_phase = [energy_phases[0]]
        for k in energy_phases[1:]:
            if corrected_phase[-1]-k > np.pi:
                corrected_phase.append(k + 2*np.pi)
            elif k-corrected_phase[-1] > np.pi:
                corrected_phase.append(k - 2*np.pi)
            else:
                corrected_phase.append(k)
        plt.plot(X,
                corrected_phase,
                alpha=0.666,
                label='{}°'.format(angle))
        plt.fill_between(X,
                        np.array(corrected_phase) +
                        np.array(phase_uncertainties_superset[angle]),
                        np.array(corrected_phase) -
                        np.array(phase_uncertainties_superset[angle]),
                        color='grey',
                        alpha=0.2)
    plt.margins(x=0)
    plt.xlabel('Photoelectron Energy (eV)', weight='normal')
    plt.ylabel('Extracted Phase (radians)', weight='normal')
    plt.grid(True)
    plt.legend(ncol=2, fancybox=True,shadow=True) 
    plt.show()

def plot_angle_integrated_phases(XUV_freq="0.9830", SB=16, path='./', component='all'):
    """
    Integrates the emission-angle resolved momentum spectrum into a 1D spectrum, 
    then calculates the spectral phase across a chosen sideband and plots the 
    spectral phase as a function of photoelectron energy.

    Arguments: 
        - XUV_freq:  <str>
                     The frequency of the central XUV harmonic used in the 
                     RABBITT calculation; this is used to load the correct
                     data sets and to identify the sideband limits.

        - SB:        <int>
                     The sideband to be investigated, identified as a multiple
                     of the driving IR frequency.

        - path:      <str>
                     The relative or absolute path to the folder 
                     containing the time-delayed photoelectron spectra
                     output from GeneraliseRecoupling.

        - component: <str>
                     The component of the photoelectron spectra to be plotted.
                     Options are: 
                         'all' - the total delay-averaged photoelectron spectrum
                         'J1/2' - only the J=1/2 Spin-Orbit component contributions
                         'J3/2' - only the J=3/2 Spin-Orbit component contributions
    Returns: 
        N/A
    """
    phase_values, phase_uncertainties, momenta = extract_1D_phases(XUV_freq, SB, path, component)

    X = mom_au_to_en_ev(np.array(momenta)) 

    plt.close('all')
    fig = plt.figure(10,figsize=(14,8))

    corrected_phase = [phase_values[0]]
    for k in phase_values[1:]:
        if corrected_phase[-1]-k > np.pi:
            corrected_phase.append(k + 2*np.pi)
        elif k-corrected_phase[-1] > np.pi:
            corrected_phase.append(k - 2*np.pi)
        else:
            corrected_phase.append(k)
    plt.plot(X,
            corrected_phase,
            alpha=0.666)
    plt.fill_between(X,
                    np.array(corrected_phase) +
                    np.array(phase_uncertainties),
                    np.array(corrected_phase) -
                    np.array(phase_uncertainties),
                    color='grey',
                    alpha=0.2)
    plt.margins(x=0)
    plt.xlabel('Photoelectron Energy (eV)', weight='normal')
    plt.ylabel('Extracted Phase (radians)', weight='normal')
    plt.grid(True)
    plt.show()


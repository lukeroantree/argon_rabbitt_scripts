import numpy as np
import os
from tqdm.auto import tqdm

"""
sb_lims is a dictionary matching hard-coded grid points 
for each sideband @ each XUV frequency (in a.u.). 

You can tweak / extend it by using 'plot_delay_averaged_sideband'
to locate the desired sideband in eV, then changing the axis
parameter to 'indices' to find the indices of the desired 
grid points.
"""

sb_lims={'all':  
            {14:{"9830": (1720,1770) # 9830 => 0.9830 a.u. XUV freq 
                    },               #      => 786.38nm driving IR
             16:{"9830": (2123,2167)
                    }
            },
         'j1/2':  
            {14:{"9830": (1720,1745)
                    },
             16:{"9830": (2123,2146)
                    }
            },
         'j3/2':  
            {14:{"9830": (1745,1769)
                    },
             16:{"9830": (2143, 2167)
                    }
            },
        }

def load_GeneraliseRecouplingData(path='./', component="All"):
    """
    Loads the photoelectron spectra data for a given calculation and
    specified component.

    Arguments:
        - path:      <str>
                    The relative or absolute path to the folder 
                    containing the time-delayed photoelectron spectra
                    output from GeneraliseRecoupling.

        - component: <str>
                    The component of the photoelectron spectra to be plotted.
                    Options are: 
                        'all' - the total delay-averaged photoelectron spectrum
                        'J1/2' - only the J=1/2 Spin-Orbit component contributions
                        'J3/2' - only the J=3/2 Spin-Orbit component contributions
    Returns: 
        - data: <list <2D numpy array>>
                The photoelectron momentum spectrum, for each emission angle,
                for each time delay.
        
        - momentum_range: <numpy array>
                          The momentum range corresponding to the photoelectron
                          momentum spectrum, in atomic units.

    """
    assert type(component) is str, "Component must be a string (All, J1/2, J3/2)"
    assert component.lower() in ('all', 'j1/2', 'j3/2'), "component must be one of (All, J1/2, J3/2)"
    assert type(path) is str, "path must be a string"
    if path[-1] != '/': 
        path += '/'
    assert os.path.isdir(path), f"path to datafiles not found: {path}"

    if component.lower() == 'all':
        prefix = "AllWeeeee"
    elif component.lower() == 'j1/2':
        prefix = "SpinDownAll3p512_"
    elif component.lower() == 'j3/2':
        prefix = "SpinDownAll3p532_"
    else:
        raise ValueError("component must be one of (All, J1/2, J3/2)")

    datafiles = [f"{path}{prefix}{0.125*(snap-8):.3f}.txt" for snap in range(17)]

    for datafile in datafiles:
        assert os.path.isfile(datafile), f"File {datafile} seems to be missing"
    assert os.path.isfile(f"{path}RecouplingScale.txt"), f"File {path}RecouplingScale.txt seems to be missing"

    data = [np.loadtxt(snap) for snap in tqdm(datafiles, leave=True, desc='Loading Data')]
    momentum_range = np.loadtxt(f'{path}RecouplingScale.txt')
    return data, momentum_range

def get_sb_lims(SB=16, XUV_freq="0.9830", component='all'):
    """
    Arguments:
        - path:      <str>
                    The relative or absolute path to the folder 
                    containing the time-delayed photoelectron spectra
                    output from GeneraliseRecoupling.

        - component: <str>
                    The component of the photoelectron spectra to be plotted.
                    Options are: 
                        'all' - the total delay-averaged photoelectron spectrum
                        'J1/2' - only the J=1/2 Spin-Orbit component contributions
                        'J3/2' - only the J=3/2 Spin-Orbit component contributions
    Returns: 
        - <tuple>: the upper and lower sideband limits, in raw grid points
    """

    assert type(SB) is int, "SB must be an integer"
    assert type(XUV_freq) is str, "XUV_freq must be a string (form 0.1234 in a.u.)"

    return sb_lims[component][SB][XUV_freq.split('.')[-1]]